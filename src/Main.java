import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

    static int EXECUTION = 100;

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);

        System.out.print("Input size: ");
        int n = s.nextInt();

        int[] array = new int[n];

        for (int i = 1; i <= array.length; i++) {
            array[i - 1] = i;
        }

//        linearSearchDisplay(array);
//        System.out.println();
        binarySearchDisplay(array);
    }

    private static void binarySearch(int[] array, int key) {
        int low = array[0];
        int high = array[array.length - 1];

        while (low <= high) {
            int mid = (low + high) / 2;
            if (array[mid] == key) {
                break;
            } else if (array[mid] > key) {
                high = mid - 1;
            } else {
                low = mid + 1;
            }
        }
    }

    private static void binarySearchDisplay(int[] array) {
        int bestCase = array.length / 2;
        int worstCase = -1;
        int averageCase = array[0];

        ArrayList<Double> bestCaseTime = new ArrayList<>();
        ArrayList<Double> averageCaseTime = new ArrayList<>();
        ArrayList<Double> worstCaseTime = new ArrayList<>();

        System.out.println("Binary Search Algorithm\n");

        for (int i = 0; i < Main.EXECUTION; i++) {
            // Best Case Scenario
            long startTime1 = System.nanoTime();
            binarySearch(array, bestCase);
            long endTime1 = System.nanoTime();
            bestCaseTime.add(((endTime1 - startTime1)/1000000.0));

            // Average Case Scenario
            long startTime2 = System.nanoTime();
            binarySearch(array, averageCase);
            long endTime2 = System.nanoTime();
            averageCaseTime.add(((endTime2 - startTime2)/1000000.0));

            // Worst Case Scenario
            long startTime3 = System.nanoTime();
            binarySearch(array, worstCase);
            long endTime3 = System.nanoTime();
            worstCaseTime.add(((endTime3 - startTime3)/1000000.0));
        }

        double averageBest = calculateAverage(bestCaseTime);
        double averageAverage = calculateAverage(averageCaseTime);
        double averageWorst = calculateAverage(worstCaseTime);

        System.out.println("Best Case Time in Milliseconds: " + BigDecimal.valueOf(averageBest).toPlainString());
        System.out.println("Average Case Time in Milliseconds: " + BigDecimal.valueOf(averageAverage).toPlainString());
        System.out.println("Worst Case Time in Milliseconds: " + BigDecimal.valueOf(averageWorst).toPlainString());
    }

    private static void linearSearch(int[] array, int key) {
        for (int i = 0; i < array.length; i++) {
            if(array[i] == key) {
                break;
            }
        }
    }

    private static void linearSearchDisplay(int[] array) {
        int bestCase = array[0];
        int worstCase = -1;
        int averageCase = array.length / 2;

        ArrayList<Double> bestCaseTime = new ArrayList<>();
        ArrayList<Double> averageCaseTime = new ArrayList<>();
        ArrayList<Double> worstCaseTime = new ArrayList<>();

        System.out.println("Linear Search Algorithm\n");

        for (int i = 0; i < Main.EXECUTION; i++) {
            // Best Case Scenario
            long startTime1 = System.nanoTime();
            linearSearch(array, bestCase);
            long endTime1 = System.nanoTime();
            bestCaseTime.add(((endTime1 - startTime1)/1000000.0));

            // Average Case Scenario
            long startTime2 = System.nanoTime();
            linearSearch(array, averageCase);
            long endTime2 = System.nanoTime();
            averageCaseTime.add(((endTime2 - startTime2)/1000000.0));

            // Worst Case Scenario
            long startTime3 = System.nanoTime();
            linearSearch(array, worstCase);
            long endTime3 = System.nanoTime();
            worstCaseTime.add(((endTime3 - startTime3)/1000000.0));
        }

        double averageBest = calculateAverage(bestCaseTime);
        double averageAverage = calculateAverage(averageCaseTime);
        double averageWorst = calculateAverage(worstCaseTime);

        System.out.println("Best Case Time in Milliseconds: " + BigDecimal.valueOf(averageBest).toPlainString());
        System.out.println("Average Case Time in Milliseconds: " + BigDecimal.valueOf(averageAverage).toPlainString());
        System.out.println("Worst Case Time in Milliseconds: " + BigDecimal.valueOf(averageWorst).toPlainString());
    }

    private static double calculateAverage(List<Double> list) {
        double sum = 0;

        if (!list.isEmpty()) {
            for (double element : list) {
                sum += element;
            }

            return sum / list.size();
        }

        return sum;
    }
}
